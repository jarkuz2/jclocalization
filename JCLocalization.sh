#!/bin/bash

#  JCLocalization.sh
#
#
#  Created by Josh Campion on 20/02/2016.
#

read_file=$1
write_swift_path=$2
write_strings_path=$3

prefix=""

key_type="${prefix}LocalizationKey"
func_name="${prefix}LocalizedString"
write_name="${prefix}Localization.swift"

# ensure the strings tsv has a new line character

sed -i -e '$a\' "${read_file}"

# populate arrays with the tsv file

declare -a keys=()
declare -a values=()
declare -a comments=()

strings_tsv=$(cat "${read_file}")
strings_tsv+="\n"

while IFS=$'\t' read -r a b c
do

keys+=( "${a}" )

#escape the necessary characters
ESCAPED="${b}"
ESCAPED="${ESCAPED//\"/\\\"}"
ESCAPED="${ESCAPED//[$][s]/\$@}"
ESCAPED="${ESCAPED//[%][s]/%@}"

values+=( "${ESCAPED}" )
comments+=( "${c}" )

done < "${read_file}"

key_count=${#keys[@]}

# write keys as an enum
enum_definition="//\n"
enum_definition+="// ${write_name}\n"
enum_definition+="//\n"
enum_definition+="// Created by JCLocalization.\n"
enum_definition+="//\n"
enum_definition+="\n"
enum_definition+="import Foundation\n"
enum_definition+="\n"
enum_definition+="public enum ${key_type}: String {\n"
enum_definition+="\n"

for (( i=0;i<$key_count;i++))
do

this_key="${keys[${i}]}"
this_value="${values[${i}]}"
this_comment="${comments[${i}]}"

enum_definition+="    /**\n"
enum_definition+="    \n"
enum_definition+="     ${this_comment}\n"
enum_definition+="    \n"
enum_definition+="    - Note: Default Value:\n"
enum_definition+="        *${this_value}*\n"
enum_definition+="    \n"
enum_definition+="    */\n"
enum_definition+="    case ${this_key}\n\n"
done

enum_definition+="\n"
enum_definition+="}\n"

# create an 'NSLocalizedString()' function equivalent. The actual values will be referenced from a .strings file.

strings_function="/**\n\n Machine generated function to load an \`NSLocalizedString(...)\` entry using a convenience enum.\n\n"
strings_function+=" - parameter key: The key to fetch a string for. The key's \`rawValue\` is used as the key in the \`.strings\` file.\n\n*/\n"
strings_function+="public func ${func_name}(_ key:${key_type}) -> String {\n"
strings_function+="    return Bundle(for: LocalizationClass.self).localizedString(forKey: key.rawValue, value: nil, table: \"JCLocalization\")\n"
strings_function+="}"

# concatenate the results

localization_file="${enum_definition}\n"
localization_file+="/// Class for accessing the JCLocaliztion bundle to reference the correct \`.strings\` file.\n"
localization_file+="public class LocalizationClass { }\n\n"
localization_file+="${strings_function}\n"

# write to a file
chmod +w "${write_swift_path}"
echo -e "${localization_file}" > "${write_swift_path}"

# Create the .strings file

echo -e "/*\n\nJCLocalization.strings\n\nCreated by JCLocalization\n\n*/\n\n" > "${write_strings_path}"

for (( i=0;i<$key_count;i++))
do

this_key="${keys[${i}]}"
this_value="${values[${i}]}"
this_comment="${comments[${i}]}"

echo "/* ${this_comment} */" >> "${write_strings_path}"
echo "\"${this_key}\" = \"${this_value}\";" >> "${write_strings_path}"
echo -e "\n" >> "${write_strings_path}"

done
