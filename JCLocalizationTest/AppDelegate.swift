//
//  AppDelegate.swift
//  JCLocalizationTest
//
//  Created by Josh Campion on 28/02/2016.
//  Copyright © 2016 Josh Campion Development. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    private func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

