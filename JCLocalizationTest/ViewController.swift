//
//  ViewController.swift
//  JCLocalizationTest
//
//  Created by Josh Campion on 28/02/2016.
//  Copyright © 2016 Josh Campion Development. All rights reserved.
//

import UIKit

@IBDesignable
class IBLabel: UILabel { }

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

