//
//  JCLocalizationTests.swift
//  JCLocalizationTests
//
//  Created by Josh Campion on 21/02/2016.
//  Copyright © 2016 Josh Campion Development. All rights reserved.
//

import XCTest
import JCLocalization

class JCLocalizationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let testString = LocalizedString(key: .Test)
        XCTAssertEqual(testString, "This is a test.")
    }

}
