//
//  UILabel+JCLocalization.swift
//  JCLocalization
//
//  Created by Josh Campion on 21/02/2016.
//  Copyright © 2016 Josh Campion Development. All rights reserved.
//

import Foundation
import ObjectiveC

public protocol Localizable {
    
    var localizationKey: LocalizationKey? { get set }
    
    var localizationKeyID:String? { get set }
}

public extension Localizable {
    
    public var localizationKey: LocalizationKey? {
        get {
            if let keyID = localizationKeyID {
                return LocalizationKey(rawValue: keyID)
            } else {
                return nil
            }
        }
        set {
            localizationKeyID = newValue?.rawValue
        }
    }
}

private var LabelLocalizationKey:UInt8 = 0

extension UILabel: Localizable {
    
    @IBInspectable
    public var localizationKeyID:String? {
        get {
            return objc_getAssociatedObject(self, &LabelLocalizationKey) as? String
        }
        set {
            
            objc_setAssociatedObject(self, &LabelLocalizationKey, newValue, .OBJC_ASSOCIATION_RETAIN)
            
            if let keyID = newValue,
                let key = LocalizationKey(rawValue: keyID) {
                    
                    self.text = LocalizedString(key)
                    
            }
        }
    }
}
